import Vue from 'vue'
import VueRouter from 'vue-router'

import Chamados from '../views/Chamados.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Chamados',
    component: Chamados
  }
]

const router = new VueRouter({
  mode: 'history',
  linkExactActiveClass: 'active',
  routes
})

export default router
