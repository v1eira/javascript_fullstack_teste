const PacienteModel = (sequelize, Sequelize) => sequelize.define('pacientes', {
  cpf: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Este campo não pode ser nulo',
      },
      notEmpty: {
        msg: 'Este campo não pode ser vazio',
      },
    },
  },
  nome: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Este campo não pode ser nulo',
      },
      notEmpty: {
        msg: 'Este campo não pode ser vazio',
      },
    },
  },
  data_nascimento: {
    type: Sequelize.DATE,
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Este campo não pode ser nulo',
      },
      notEmpty: {
        msg: 'Este campo não pode ser vazio',
      },
      isDate: {
        msg: 'A data não é válida',
      },
    },
  },
  sexo: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Este campo não pode ser nulo',
      },
      notEmpty: {
        msg: 'Este campo não pode ser vazio',
      },
    },
  },
  telefone: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Este campo não pode ser nulo',
      },
      notEmpty: {
        msg: 'Este campo não pode ser vazio',
      },
    },
  },
  id_plano: {
    type: Sequelize.INTEGER,
    allowNull: true,
    validate: {
      isNumeric: {
        msg: 'ID inválido',
      },
    },
  },
});

module.exports = PacienteModel;
