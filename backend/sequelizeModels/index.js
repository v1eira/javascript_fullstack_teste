const Sequelize = require('sequelize');
const config = require('../config')().sequelize;

const sequelize = new Sequelize(config);

const database = {
  Sequelize,
  sequelize,
};

database.planos = require('./PlanoModel')(sequelize, Sequelize);
database.pacientes = require('./PacienteModel')(sequelize, Sequelize);

database.pacientes.belongsTo(database.planos, {
  foreignKey: 'id_plano',
  as: 'plano',
});

module.exports = database;
