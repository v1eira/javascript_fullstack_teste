const PlanoModel = (sequelize, Sequelize) => sequelize.define('planos', {
  descricao: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Este campo não pode ser nulo',
      },
      notEmpty: {
        msg: 'Este campo não pode ser vazio',
      },
    },
  },
});

module.exports = PlanoModel;
