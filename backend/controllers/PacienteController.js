const database = require('../sequelizeModels');

const PacienteController = () => {
  const Paciente = database.pacientes;
  const Plano = database.planos;

  /**
   * Cria um paciente
   * @returns {Promise<void>}
   */
  const cria = async (req, res) => {
    try {
      const cpfExiste = await Paciente.findOne({
        where: { cpf: req.body.cpf },
      });

      if (cpfExiste) {
        return res.status(400).json({ error: 'Já existe um usuário com o CPF informado.' });
      }

      if (req.body.id_plano) {
        const planoExiste = await Plano.findOne({
          where: { id: req.body.id_plano },
        });

        if (!planoExiste) {
          return res.status(400).json({ error: 'O plano informado não existe.' });
        }
      }

      const paciente = await Paciente.create(req.body);
      return res.json(paciente);
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Lista pacientes
   * @returns {Promise<void>}
   */
  const lista = async (req, res) => {
    try {
      const pacientes = await Paciente.findAll({
        include: [
          {
            model: Plano,
            as: 'plano',
            attributes: ['descricao'],
          },
        ],
      });
      return res.json(pacientes);
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Atualiza um paciente
   * @returns {Promise<void>}
   */
  const atualiza = async (req, res) => {
    try {
      const { id } = req.params;

      if (!Number.isInteger(Number(id))) {
        return res.status(400).json({ error: 'ID inválido.' });
      }

      const paciente = await Paciente.findByPk(id);

      if (!paciente) {
        return res.status(404).json({ error: 'Paciente não existe.' });
      }

      if (req.body.cpf && (req.body.cpf !== paciente.cpf)) {
        const cpfExiste = await Paciente.findOne({
          where: { cpf: req.body.cpf },
        });

        if (cpfExiste) {
          return res.status(400).json({ error: 'Já existe um usuário com o CPF informado.' });
        }
      }

      if (req.body.id_plano && (req.body.id_plano !== paciente.id_plano)) {
        const planoExiste = await Plano.findOne({
          where: { id: req.body.id_plano },
        });

        if (!planoExiste) {
          return res.status(400).json({ error: 'O plano informado não existe.' });
        }
      }

      await paciente.update(req.body);

      return res.json(paciente);
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Deleta um paciente
   * @returns {Promise<void>}
   */
  const deleta = async (req, res) => {
    try {
      const { id } = req.params;

      if (!Number.isInteger(Number(id))) {
        return res.status(400).json({ error: 'ID inválido.' });
      }

      const paciente = await Paciente.findByPk(id);

      if (!paciente) {
        return res.status(404).json({ error: 'Paciente não existe.' });
      }

      await paciente.destroy();

      return res.send({ message: 'Paciente deletado com sucesso.' });
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Mostra um paciente
   * @returns {Promise<void>}
   */
  const mostra = async (req, res) => {
    try {
      const { id } = req.params;

      if (!Number.isInteger(Number(id))) {
        return res.status(400).json({ error: 'ID inválido.' });
      }

      const paciente = await Paciente.findByPk(id, {
        include: [
          {
            model: Plano,
            as: 'plano',
            attributes: ['descricao'],
          },
        ],
      });

      if (!paciente) {
        return res.status(404).json({ error: 'Paciente não existe.' });
      }

      return res.json(paciente);
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  return {
    cria,
    lista,
    atualiza,
    deleta,
    mostra,
  };
};

module.exports = PacienteController;
