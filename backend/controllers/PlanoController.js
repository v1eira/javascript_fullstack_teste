const database = require('../sequelizeModels');

const PlanoController = () => {
  const Plano = database.planos;

  /**
   * Cria um plano
   * @returns {Promise<void>}
   */
  const cria = async (req, res) => {
    try {
      const plano = await Plano.create(req.body);
      return res.json(plano);
    } catch (error) {
      return res.status(400).json({ error });
    }
  };

  /**
   * Lista planos
   * @returns {Promise<void>}
   */
  const lista = async (req, res) => {
    try {
      const planos = await Plano.findAll();
      return res.json(planos);
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Atualiza um plano
   * @returns {Promise<void>}
   */
  const atualiza = async (req, res) => {
    try {
      const { id } = req.params;

      if (!Number.isInteger(Number(id))) {
        return res.status(400).json({ error: 'ID inválido' });
      }

      const plano = await Plano.findByPk(id);

      if (!plano) {
        return res.status(404).json({ error: 'Plano não existe.' });
      }

      await plano.update(req.body);

      return res.json(plano);
    } catch (error) {
      return res.status(400).json({ error });
    }
  };

  /**
   * Deleta um plano
   * @returns {Promise<void>}
   */
  const deleta = async (req, res) => {
    try {
      const { id } = req.params;

      if (!Number.isInteger(Number(id))) {
        return res.status(400).json({ error: 'ID inválido' });
      }

      const plano = await Plano.findByPk(id);

      if (!plano) {
        return res.status(404).json({ error: 'Plano não existe.' });
      }

      await plano.destroy();

      return res.send({ message: 'Plano deletado com sucesso.' });
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Mostra um plano
   * @returns {Promise<void>}
   */
  const mostra = async (req, res) => {
    try {
      const { id } = req.params;

      if (!Number.isInteger(Number(id))) {
        return res.status(400).json({ error: 'ID inválido' });
      }

      const plano = await Plano.findByPk(id);

      if (!plano) {
        return res.status(404).json({ error: 'Plano não existe.' });
      }

      return res.json(plano);
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  return {
    cria,
    lista,
    atualiza,
    deleta,
    mostra,
  };
};

module.exports = PlanoController;
