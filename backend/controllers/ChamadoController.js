/* eslint-disable no-underscore-dangle */
const elasticsearch = require('@elastic/elasticsearch');
const Yup = require('yup');
const { v4: uuidv4 } = require('uuid');
const database = require('../sequelizeModels');

const ChamadoController = (app) => {
  const elasticsearchConfig = app.config.elasticsearch;
  const { MotivoModel } = app.models;
  const Paciente = database.pacientes;
  const Plano = database.planos;

  const client = new elasticsearch.Client({
    node: elasticsearchConfig.host,
  });

  /**
   * Cria um chamado
   * @returns {Promise<void>}
   */
  const cria = async (req, res) => {
    try {
      const schema = Yup.object().shape({
        id_paciente: Yup.number().integer().required(),
        id_motivo: Yup.number().integer().required(),
        descricao: Yup.string().required(),
        status: Yup.string().required(),
      });

      if (!(await schema.isValid(req.body))) {
        return res.status(400).json({ error: 'Erro de validação! Confira os campos informados.' });
      }

      // verifica se o motivo informado existe
      const motivos = await MotivoModel.lista();
      const motivoExiste = motivos.some((motivo) => motivo.id_motivo === req.body.id_motivo);

      if (!motivoExiste) {
        return res.status(400).json({ error: 'O motivo informado não existe.' });
      }

      // verifica se o paciente existe
      const paciente = await Paciente.findByPk(req.body.id_paciente);

      if (!paciente) {
        return res.status(404).json({ error: 'Paciente não existe.' });
      }

      const dadosChamado = {
        id_paciente: req.body.id_paciente,
        nome_paciente: paciente.nome,
        id_motivo: req.body.id_motivo,
        numero_chamado: uuidv4(),
        descricao: req.body.descricao,
        status: req.body.status,
        data_criacao: new Date().toJSON(),
      };

      const chamado = await client.index({
        index: elasticsearchConfig.index,
        type: 'chamado',
        body: { ...dadosChamado },
      });

      return res.status(200).json({
        id: chamado.body._id,
        ...dadosChamado,
      });
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Lista chamados
   * @returns {Promise<void>}
   */
  const lista = async (req, res) => {
    const { busca } = req.query;
    const { pagina = 1 } = req.query;

    try {
      let search;
      if (busca) {
        search = await client.search({
          index: elasticsearchConfig.index,
          body: {
            query: {
              multi_match: {
                query: busca,
                fields: ['numero_chamado', 'status', 'id_paciente', 'nome_paciente', 'descricao'],
              },
            },
          },
          size: 10,
          from: (pagina - 1) * 10,
        });
      } else {
        search = await client.search({
          index: elasticsearchConfig.index,
          body: {
            query: {
              match_all: {},
            },
          },
          size: 10,
          from: (pagina - 1) * 10,
        });
      }

      const total = search.body.hits.total.value;
      const chamados = search.body.hits.hits.map((c) => ({
        id: c._id,
        ...c._source,
      }));

      return res.status(200).json({
        total,
        chamados,
      });
    } catch (error) {
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Atualiza um chamado
   * @returns {Promise<void>}
   */
  const atualiza = async (req, res) => {
    try {
      const { id } = req.params;

      const schema = Yup.object().shape({
        id_paciente: Yup.number().integer(),
        id_motivo: Yup.number().integer(),
        descricao: Yup.string(),
        status: Yup.string(),
      });

      if (!(await schema.isValid(req.body))) {
        return res.status(400).json({ error: 'Erro de validação! Confira os campos informados.' });
      }

      // verifica se o motivo informado existe
      if (req.body.id_motivo) {
        const motivos = await MotivoModel.lista();
        const motivoExiste = motivos.some((motivo) => motivo.id_motivo === req.body.id_motivo);

        if (!motivoExiste) {
          return res.status(400).json({ error: 'O motivo informado não existe.' });
        }
      }

      const dadosChamado = {
        id_paciente: req.body.id_paciente,
        id_motivo: req.body.id_motivo,
        descricao: req.body.descricao,
        status: req.body.status,
      };

      // verifica se o paciente existe
      let paciente;
      if (req.body.id_paciente) {
        paciente = await Paciente.findByPk(req.body.id_paciente);

        if (!paciente) {
          return res.status(404).json({ error: 'Paciente não existe.' });
        }

        dadosChamado.nome_paciente = paciente.nome;
      }

      const chamadoAtualizado = await client.update({
        index: elasticsearchConfig.index,
        id,
        body: {
          doc: { ...dadosChamado },
        },
        _source: true,
      });

      return res.status(200).json({
        id,
        ...chamadoAtualizado.body.get._source,
      });
    } catch (error) {
      if (error.meta.statusCode === 404) {
        return res.status(404).json({ error: 'O chamado informado não existe.' });
      }
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Deleta um chamado
   * @returns {Promise<void>}
   */
  const deleta = async (req, res) => {
    try {
      const { id } = req.params;

      await client.delete({
        index: elasticsearchConfig.index,
        id,
      });

      return res.status(200).json({ msg: 'Chamado deletado com sucesso!' });
    } catch (error) {
      if (error.meta.statusCode === 404) {
        return res.status(404).json({ error: 'O chamado informado não existe.' });
      }
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  /**
   * Mostra um chamado
   * @returns {Promise<void>}
   */
  const mostra = async (req, res) => {
    try {
      const { id } = req.params;

      const chamado = await client.get({
        index: elasticsearchConfig.index,
        id,
      });

      const paciente = await Paciente.findByPk(chamado.body._source.id_paciente, {
        include: [
          {
            model: Plano,
            as: 'plano',
            attributes: ['descricao'],
          },
        ],
      });
      const listaMotivos = await MotivoModel.lista();
      const motivo = listaMotivos.filter((m) => m.id_motivo === chamado.body._source.id_motivo);

      return res.status(200).json({
        ...chamado.body._source,
        paciente,
        motivo: motivo[0],
      });
    } catch (error) {
      if (error.meta.statusCode === 404) {
        return res.status(404).json({ error: 'O chamado informado não existe.' });
      }
      return res.status(500).json({ error: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  return {
    cria,
    lista,
    atualiza,
    deleta,
    mostra,
  };
};

module.exports = ChamadoController;
