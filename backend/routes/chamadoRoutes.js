module.exports = (app) => {
  const { ChamadoController } = app.controllers;

  /**
   * Cadastra um novo chamado.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const criaChamado = async (req, res) => ChamadoController.cria(req, res);
  app.route('/chamado').post(criaChamado);

  /**
   * Lista os chamados.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const listaChamados = async (req, res) => ChamadoController.lista(req, res);
  app.route('/chamado').get(listaChamados);

  /**
   * Atualiza um chamado.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const atualizaChamado = async (req, res) => ChamadoController.atualiza(req, res);
  app.route('/chamado/:id').put(atualizaChamado);

  /**
   * Deleta um chamado.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const deletaChamado = async (req, res) => ChamadoController.deleta(req, res);
  app.route('/chamado/:id').delete(deletaChamado);

  /**
   * Mostra um chamado.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const mostraChamado = async (req, res) => ChamadoController.mostra(req, res);
  app.route('/chamado/:id').get(mostraChamado);
};
