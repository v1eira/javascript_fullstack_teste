module.exports = (app) => {
  const { PlanoController } = app.controllers;

  /**
   * Cadastra um novo plano.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const criaPlano = async (req, res) => PlanoController.cria(req, res);
  app.route('/plano').post(criaPlano);

  /**
   * Lista os planos.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const listaPlanos = async (req, res) => PlanoController.lista(req, res);
  app.route('/plano').get(listaPlanos);

  /**
   * Atualiza um plano.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const atualizaPlano = async (req, res) => PlanoController.atualiza(req, res);
  app.route('/plano/:id').put(atualizaPlano);

  /**
   * Deleta um plano.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const deletaPlano = async (req, res) => PlanoController.deleta(req, res);
  app.route('/plano/:id').delete(deletaPlano);

  /**
   * Mostra um plano.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const mostraPlano = async (req, res) => PlanoController.mostra(req, res);
  app.route('/plano/:id').get(mostraPlano);
};
