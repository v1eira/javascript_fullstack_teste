module.exports = (app) => {
  const { PacienteController } = app.controllers;

  /**
   * Cadastra um novo paciente.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const criaPaciente = async (req, res) => PacienteController.cria(req, res);
  app.route('/paciente').post(criaPaciente);

  /**
   * Lista os pacientes.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const listaPacientes = async (req, res) => PacienteController.lista(req, res);
  app.route('/paciente').get(listaPacientes);

  /**
   * Atualiza um paciente.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const atualizaPaciente = async (req, res) => PacienteController.atualiza(req, res);
  app.route('/paciente/:id').put(atualizaPaciente);

  /**
   * Deleta um paciente.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const deletaPaciente = async (req, res) => PacienteController.deleta(req, res);
  app.route('/paciente/:id').delete(deletaPaciente);

  /**
   * Mostra um paciente.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const mostraPaciente = async (req, res) => PacienteController.mostra(req, res);
  app.route('/paciente/:id').get(mostraPaciente);
};
