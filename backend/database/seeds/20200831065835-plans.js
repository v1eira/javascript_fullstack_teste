module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert('planos',
    [
      {
        id: 1,
        descricao: 'Plano 1',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 2,
        descricao: 'Plano 2',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 3,
        descricao: 'Plano 3',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 4,
        descricao: 'Plano 4',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 5,
        descricao: 'Plano 5',
        created_at: new Date(),
        updated_at: new Date(),
      },
    ], {}),

  down: (queryInterface) => queryInterface.bulkDelete('planos', null, {}),
};
