module.exports = {
  dialect: 'mysql',
  host: 'localhost',
  port: 3307,
  username: 'root',
  password: 'teste',
  database: 'mobilesaude',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
