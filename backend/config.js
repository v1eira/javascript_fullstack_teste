module.exports = () => ({
  port: 3100,
  redis: {
    password: 'a4d0985e57073d9927ded659ed447e17fac33f99',
    port: 6379,
    host: 'homologacao.tapcore.com.br',
    db: 0,
  },
  sequelize: {
    dialect: 'mysql',
    host: 'localhost',
    port: 3307,
    username: 'root',
    password: 'teste',
    database: 'mobilesaude',
    define: {
      timestamps: true,
      underscored: true,
      underscoredAll: true,
    },
  },
  elasticsearch: {
    host: 'https://search-teste-fullstack-js-2v3vh2ph5iuthuvawkrfwpfwnq.sa-east-1.es.amazonaws.com',
    index: 'ms_chamados_teste_20',
  },
});
